<div>
    <div class="buttons d-flex">
        <a class="btn btn-xs btn-outline-primary mr-1" wire:click.prevent="loadEditLink">
            <i class="fa fa-pencil"></i>
        </a>
        <a class="btn btn-xs btn-outline-danger" wire:click.prevent="deleteConfirmation">
            <i class="fa fa-trash"></i>
        </a>
    </div>
</div>

@push('scripts')
    <script>
        window.addEventListener('swallinks', event => {
            Swal.fire({
                icon: 'warning',
                title: '¿Desea eliminar el enlace?',
                text: 'Esta acción no se podrá revertir',
                showConfirmButton: true,
                showCancelButton: true,
                confirmButtonText: 'Sí',
                cancelButtonText: 'Cancelar',
                confirmButtonColor: 'success'
            }).then((result) => {
                if(result.isConfirmed){
                    Livewire.emit('deleteLinkFromWidget'+event.detail.id);
                }
            });
        });
    </script>
@endpush
