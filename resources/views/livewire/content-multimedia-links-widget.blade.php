<div class="widget-box widget-color-blue ui-sortable-handle mb-4">
    <div class="widget-header widget-header-small">
        <h6 class="widget-title smaller">Enlaces relacionados</h6>
    </div>
    <div class="widget-body">
        <div class="widget-main">
            @livewire('content-multimedia-links::content-multimedia-links-widget-table', ['contentId' => $contentId])
            <div class="text-right">
                <a class="btn btn-primary" id="addLinkForm" data-toggle="modal" data-target="#modalLinks">
                    <i class="fa fa-plus-circle"></i> Añadir enlace
                </a>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalLinks" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        Añadir enlace
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-modal">
                        <input type="hidden" id="idLink" value="{{$idLink}}">
                        <div class="form-group">
                            <label for="link-title" class="control-label">Título</label>
                            <input type="text" id="link-title" name="linkTitle" class="form-control" value="{{$title}}">
                        </div>
                        <div class="form-group">
                            <label for="link-link" class="control-label">Enlace</label>
                            <input type="text" id="link-link" name="linkLink" class="form-control" value="{{$link}}">
                        </div>
                        <div class="form-group form-check form-check-modal">
                            <input type="checkbox" class="form-check-input" id="link-active" @if($active) checked @endif>
                            <label class="form-check-label" for="link-active">Activo</label>
                        </div>
                        <div class="form-group form-check form-check-modal">
                            <input type="checkbox" class="form-check-input" id="link-featured" @if($featured) checked @endif>
                            <label class="form-check-label" for="link-featured">Destacado</label>
                        </div>
                        <div class="form-group form-check form-check-modal">
                            <input type="checkbox" class="form-check-input" id="link-external" @if($external) checked @endif>
                            <label class="form-check-label" for="link-external">Enlace externo</label>
                        </div>
                        <div class="form-group text-center">
                            <button class="btn btn-primary w-100" id="guardar" wire:click.prevent>Guardar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
    @vite('vendor/bittacora/bpanel4-panel/resources/assets/js/livewire-sortable.js')
    <script>
        var boton = document.getElementById('guardar');

        boton.addEventListener('click', function(){
            $valueFields = linksFieldsTreatment();
            Livewire.emit('saveContentMultimediaLink', $valueFields[0], $valueFields[1], $valueFields[2], $valueFields[3], $valueFields[4]);
        });

        window.addEventListener('closeContentMultimediaLinksWidgetModal', function(){
            $("#modalLinks").modal("hide");
            linksFieldsTreatment(true);
        });

        window.addEventListener('openEditContentMultimediaLink', function(){
            $("#modalLinks").modal("show");
            hideCheckboxes(true);
        });

        $("#modalLinks").on("hidden.bs.modal", function(){
            Livewire.emit("clearParameters");
        });

        function linksFieldsTreatment($clear = false){
            if($clear){

                $('#link-title').val("");
                $('#link-link').val("");
                $('#idLink').val("");
                $("#link-active").prop("checked", false);
                $("#link-featured").prop("checked", false);
                $("#link-external").prop("checked", false);
                hideCheckboxes();
            }else{
                var linkTitle = $('#link-title').val();
                var linkLink = $('#link-link').val();
                var linkActive = $('#link-active').prop("checked");
                var linkFeatured = $('#link-featured').prop("checked");
                var linkExternal = $('#link-external').prop("checked");

                return [linkTitle, linkLink, linkActive, linkFeatured, linkExternal];
            }
        }

        function hideCheckboxes($hide = false){
            if($hide){
                $(".form-check-modal").addClass("d-none");
            }else{
                $(".form-check-modal").removeClass("d-none");
            }
        }
    </script>
@endpush
