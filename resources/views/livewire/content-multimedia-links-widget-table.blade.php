<x-livewire-tables::bs4.table.cell>
    {{$row->title}}
</x-livewire-tables::bs4.table.cell>
<x-livewire-tables::bs4.table.cell>
    <div class="">
        <a href="{{$row->link}}">{{$row->link}}</a>
    </div>
</x-livewire-tables::bs4.table.cell>
<x-livewire-tables::bs4.table.cell>
    @livewire('utils::datatable-default', ['fieldName' => 'active', 'model' => $row, 'value' => $row->active, 'size' => 'xxs'], key('active-links-'.$row->id))
</x-livewire-tables::bs4.table.cell>
<x-livewire-tables::bs4.table.cell>
    @livewire('utils::datatable-default', ['fieldName' => 'featured', 'model' => $row, 'value' => $row->featured, 'size' => 'xxs'], key('featured-links-'.$row->id))
</x-livewire-tables::bs4.table.cell>
<x-livewire-tables::bs4.table.cell>
    @livewire('utils::datatable-default', ['fieldName' => 'external', 'model' => $row, 'value' => $row->external, 'size' => 'xxs'], key('external-links-'.$row->id))
</x-livewire-tables::bs4.table.cell>
<x-livewire-tables::bs4.table.cell>
    <div class="d-flex justify-content-center align-items-center">
        <span class="align-items-center badge bgc-purple-d1 pos-rel text-white radius-4 px-3">
            <span class="bgc-primary-tp4 opacity-5 position-tl h-100 w-100 radius-4"></span>
            <span class="pos-rel">
                {{$row->order_column}}
            </span>
        </span>
    </div>
</x-livewire-tables::bs4.table.cell>
<x-livewire-tables::bs4.table.cell>
    @livewire('content-multimedia-links::content-multimedia-links-widget-action-buttons', ['idLink' => $row->id, 'contentId' => $row->content_id], key('edit-button-link-'.$row->id))
</x-livewire-tables::bs4.table.cell>
