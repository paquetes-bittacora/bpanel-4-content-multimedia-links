<?php

namespace Bittacora\ContentMultimediaLinks;

use Bittacora\ContentMultimediaLinks\Http\Livewire\ContentMultimediaLinksWidget;
use Bittacora\ContentMultimediaLinks\Http\Livewire\ContentMultimediaLinksWidgetActionButtons;
use Bittacora\ContentMultimediaLinks\Http\Livewire\ContentMultimediaLinksWidgetTable;
use Livewire;
use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;
use Bittacora\ContentMultimediaLinks\Commands\ContentMultimediaLinksCommand;

class ContentMultimediaLinksServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {
        /*
         * This class is a Package Service Provider
         *
         * More info: https://github.com/spatie/laravel-package-tools
         */
        $package
            ->name('content-multimedia-links')
            ->hasConfigFile()
            ->hasViews()
            ->hasMigration('create_content-multimedia-links_table')
            ->hasCommand(ContentMultimediaLinksCommand::class);
    }

    public function register()
    {
        $this->app->bind('content-multimedia-links', function($app){
            return new ContentMultimediaLinks();
        });
    }

    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/../resources/views/', 'content-multimedia-links');
        Livewire::component('content-multimedia-links::content-multimedia-links-widget', ContentMultimediaLinksWidget::class);
        Livewire::component('content-multimedia-links::content-multimedia-links-widget-table', ContentMultimediaLinksWidgetTable::class);
        Livewire::component('content-multimedia-links::content-multimedia-links-widget-action-buttons', ContentMultimediaLinksWidgetActionButtons::class);
    }
}
