<?php

namespace Bittacora\ContentMultimediaLinks\Http\Livewire;

use Bittacora\ContentMultimediaLinks\Models\ContentMultimediaLinksModel;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Livewire\Component;

class ContentMultimediaLinksWidget extends Component
{
    use LivewireAlert;

    public ?int $idLink = null;
    public int $contentId;
    public ?string $title;
    public ?string $link;
    public bool $active;
    public bool $featured;
    public bool $external;

    protected $listeners = ["saveContentMultimediaLink", "loadEditLinkInformation", "clearParameters"];

    public function render()
    {
        return view('content-multimedia-links::livewire.content-multimedia-links-widget');
    }

    public function addContentMultimediaLink(){
        $this->dispatchBrowserEvent('sendContentMultimediaLink');
    }

    public function saveContentMultimediaLink($title, $link, $active, $featured, $external){
        $params = [
            'content_id' => $this->contentId,
            'title' => $title,
            'link' => $link,
            'active' => (bool) $active,
            'featured' => (bool) $featured,
            'external' => (bool) $external
        ];

        if(is_null($this->idLink)){
            $result = ContentMultimediaLinksModel::create($params);
        }else{
            $result = ContentMultimediaLinksModel::where('id', $this->idLink)->update($params);
        }

        if($result){
            if(is_null($this->idLink)){
                $this->alert('success', 'Enlace añadido');
            }else{
                $this->alert('success', 'Enlace actualizado');
            }

            $this->emit('$refresh');
            $this->emit('refreshContentMultimediaLinksWidgetTable');
            $this->emit('refreshContentMultimediaLinksWidgetActionButtons');

        }else{
            $this->alert('error', 'Error');
        }

        $this->dispatchBrowserEvent('closeContentMultimediaLinksWidgetModal');
    }

    public function loadEditLinkInformation($contentMultimediaLink){
        $this->contentId = $contentMultimediaLink['content_id'];
        $this->title = $contentMultimediaLink['title'];
        $this->link = $contentMultimediaLink['link'];
        $this->active = (bool) $contentMultimediaLink['active'];
        $this->featured = (bool) $contentMultimediaLink['featured'];
        $this->external = (bool) $contentMultimediaLink['external'];
        $this->idLink = $contentMultimediaLink['id'];

        $this->dispatchBrowserEvent('openEditContentMultimediaLink');
        $this->emit('$refresh');
    }

    public function clearParameters(){
        $this->title = null;
        $this->link = null;
        $this->active = false;
        $this->featured = false;
        $this->external = false;
        $this->idLink = null;
        $this->mount();
    }

}
