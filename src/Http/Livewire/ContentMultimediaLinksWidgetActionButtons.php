<?php

namespace Bittacora\ContentMultimediaLinks\Http\Livewire;

use Bittacora\ContentMultimediaLinks\Models\ContentMultimediaLinksModel;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Livewire\Component;
use function view;

class ContentMultimediaLinksWidgetActionButtons extends Component
{
    use LivewireAlert;

    public int $idLink;
    public int $contentId;

    protected $listeners = ['deleteLinkFromWidget'];

    protected function getListeners()
    {
        return ['deleteLinkFromWidget'.$this->idLink => 'deleteLinkFromWidget'];
    }


    public function render()
    {
        return view('content-multimedia-links::livewire.content-multimedia-links-widget-action-buttons');
    }

    public function loadEditLink(){
        $contentMultimediaLink = ContentMultimediaLinksModel::where('id', $this->idLink)->first();
        $this->emit('loadEditLinkInformation', $contentMultimediaLink);
    }

    public function deleteConfirmation(){
        $this->dispatchBrowserEvent("swallinks", [
            'type' => 'warning',
            'title' => '¿Estás seguro?',
            'text' => 'Esta acción no se podrá deshacer',
            'id' => $this->idLink
        ]);
    }

    public function deleteLinkFromWidget(){
        $result = ContentMultimediaLinksModel::where('id', $this->idLink) -> delete();
        if($result){
            $items = ContentMultimediaLinksModel::where('content_id', $this->contentId)->orderBy('order_column', 'ASC')->pluck('id');
            ContentMultimediaLinksModel::setNewOrder($items);
            $this->alert('success', 'Enlace eliminado');
            $this->emit('refreshContentMultimediaLinksWidgetTable');
        }else{
            $this->alert('error', 'Error al eliminar');
        }
    }
}
