<?php

namespace Bittacora\ContentMultimediaLinks\Http\Livewire;

use Bittacora\ContentMultimediaLinks\Models\ContentMultimediaLinksModel;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

class ContentMultimediaLinksWidgetTable extends DataTableComponent
{

    public int $contentId;
    public bool $reordering = true;
    public string $reorderingMethod = 'reorder';
    public bool $showPerPage = false;
    public bool $showPagination = false;
    public bool $showSearch = false;
    public array $perPageAccepted = [0];
    protected $listeners = ['refreshContentMultimediaLinksWidgetTable' => '$refresh'];

    public function columns(): array
    {
        return [
            Column::make('Título')->addClass('w-20'),
            Column::make('Enlace')->addClass('w-30'),
            Column::make('Activo')->addClass('w-10 text-center'),
            Column::make('Destacado')->addClass('w-10 text-center'),
            Column::make('Enlace externo')->addClass('w-10 text-center'),
            Column::make('Orden')->addClass('w-10 text-center'),
            Column::make('')->addClass('w-10')
        ];
    }

    public function query()
    {
        return ContentMultimediaLinksModel::query()->where('content_id', $this->contentId)->orderBy('order_column', 'ASC');
    }

    public function rowView(): string
    {
        return 'content-multimedia-links::livewire.content-multimedia-links-widget-table';
    }

    public function reorder($list){
        foreach($list as $item){
            ContentMultimediaLinksModel::where('id', $item['value'])->update(['order_column' => $item['order']]);
        }
    }
}
