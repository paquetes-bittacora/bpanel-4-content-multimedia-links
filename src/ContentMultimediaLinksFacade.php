<?php

namespace Bittacora\ContentMultimediaLinks;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Bittacora\ContentMultimediaLinks\ContentMultimediaLinks
 */
class ContentMultimediaLinksFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'content-multimedia-links';
    }
}
