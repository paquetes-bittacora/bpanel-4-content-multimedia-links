<?php

namespace Bittacora\ContentMultimediaLinks\Commands;

use Illuminate\Console\Command;

class ContentMultimediaLinksCommand extends Command
{
    public $signature = 'content-multimedia-links';

    public $description = 'My command';

    public function handle()
    {
        $this->comment('All done');
    }
}
